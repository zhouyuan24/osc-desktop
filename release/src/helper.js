var help = {};

help.alloy = (function() {
	return {
		createWindow: function(url, data) {
			if (url.indexOf("?") < 0) {
				url += "?";
			}
			for (var d in data) {
				url += "&" + d + "=" + data[d];
			}
			AlloyDesktop.createWindow(AlloyDesktop.getCurrentDirectory() + url.replace("?&", "?"), '', 1);
		},

		getParam: function(name) {
			var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
			var r = window.location.search.substr(1).match(reg); //匹配目标参数
			if (r != null) return unescape(r[2]);
			return null;
		},

		hoverTip : function(dom){
			var text = $(dom).attr('tip');
			AlloyDesktop.runAppEx(AlloyDesktop.getCurrentDirectory() + 'tip/index.app', text);
		}
	};
})();