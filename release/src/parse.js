var Parse = {
	defaultPhoto: "http://my.oschina.net/img/portrait.gif"
};

Parse.result = function(data) {
	var result = $(data).find('result');
	return {
		code: $(result).children('errorcode').text(),
		message: $(result).children('errormessage').text()
	}
}

var user = {};

user.parse = function(xml) {
	var u = $(xml).find('user');
	return {
		uid: $(u).children('uid').text(),
		name: $(u).children('name').text(),
		location: $(u).children('location').text(),
		followers: $(u).children('followers').text(),
		fans: $(u).children('fans').text(),
		score: $(u).children('score').text(),
		portrait: $(u).children('portrait').text()
	};
}

var Ptweet = {};

Ptweet.parseList = function(xml) {
	var list = new Array();
	var tws = $(xml).find('tweet').each(function() {
		var body = $(this).children('body').text();

		var imgReg = /<img.*?(?:>|\/>)/gi;
		var srcReg = /src=[\'\"]?([^\'\"]*)[\'\"]?/i;
		var imgs = body.match(imgReg);
		if (imgs != null) {
			for (var i = 0; i < imgs.length; i++) {
				var src = arr[i].match(srcReg);
				console(src);
			}
		}
		var t = {
			'id': $(this).children('id').text(),
			'portrait': $(this).children('portrait').text(),
			'author': $(this).children('author').text(),
			'authorid': $(this).children('authorid').text(),
			'body': $(this).children('body').text(),
			'appclient': $(this).children('appclient').text(),
			'commentcount': $(this).children('commentcount').text(),
			'pubdate': $(this).children('pubdate').text(),
			'imgsmall': $(this).children('imgSmall').text(),
			'imgbig': $(this).children('imgBig').text()
		};
		if (!t.portrait) {
			t.portrait = Parse.defaultPhoto;
		}
		list[list.length] = t;
	});
	return list;
}

/**
	<li link='http://my.oschina.net/u/942318/tweet/2235554'>

			<a href="http://my.oschina.net/u/942318" target="_blank" class='portrait'><img src="http://static.oschina.net/uploads/user/471/942318_50.jpg?t=1358941291000" align="absmiddle" alt="好奇害死猫" title="好奇害死猫" class="SmallPortrait" user="942318"/></a>

			<div class='tweet'>

			<p class='txt'><a href="http://my.oschina.net/u/942318" target="_blank">好奇害死猫</a> : 为什么不理我了，我都愁死了</p>

						<p class='outline'>发布于 2分钟前 (<a href="http://my.oschina.net/u/942318/tweet/2235554" target="_blank">0评</a>)				

							</p>

			</div>

		</li>
 */
Ptweet.parseListFromHtml = function(html) {
	//console.info(html);
	var d = document.createElement("div");
	d.innerHTML = html;
	//var t = $("<img src='/img/xx.jpg'>");
	var list = new Array();
	$(d).find('li').each(function() {
		var link = $(this).attr('link');
		var links = link.split('\/');
		var id = links[links.length - 1];
		var user_a = $(this).find('a.portrait')[0];
		var user = $(user_a).attr('href');
		var user_split = user.split('\/');
		var userid = user_split[user_split.length - 1];
		var portrait = $(user_a).find('img').attr('src');
		portrait = portrait.indexOf(URL.HTTP) < 0 ? "http://www.oschina.net" + portrait : portrait;
		var username = $(this).find('p.txt').find('a:first').text();
		$(this).find('p.txt').find('a:first').remove();
		var body = $(this).find('p.txt').html().substring(3);

		var imgReg = /<img.*?(?:>|\/>)/gi;
		var srcReg = /src=[\'\"]?([^\'\"]*)[\'\"]?/i;
		var imgs = body.match(imgReg);
		if (imgs != null) {
			for (var i = 0; i < imgs.length; i++) {
				var src = imgs[i].match(srcReg);
				console.info(src);
				body = body.replace(src[1],"http://www.oschina.net"+src[1]);
			}
		}

		var outline = $(this).find('p.outline').text();
		var outlines = outline.trim().split(" ");
		var pubdate = outlines[1];
		var commentcount = outlines[2].match(/\d+/)[0];
		var t = {
			'id': id,
			'portrait': portrait,
			'author': username,
			'authorid': userid,
			'body': body,
			'appclient': 1,
			'commentcount': commentcount,
			'pubdate': pubdate,
			'imgsmall': '',
			'imgbig': ''
		};
		if (!t.portrait) {
			t.portrait = Parse.defaultPhoto;
		}
		list[list.length] = t;
	});
	$(d).remove();
	return list;
}

Ptweet.parseNewListFromHtml = function(html) {
	var d = document.createElement("div");
	d.innerHTML = html;
	var list = new Array();
	$(d).find('li').each(function() {
		var id = $(this).attr('log');
		var user_a = $(this).find('span.user').find('a').attr('href');
		var user_split = user_a.split('\/');
		var userid = user_split[user_split.length - 1];
		var username = $(this).find('span.user').find('a').text();
		var portrait = $(this).find('span.portrait').find('img').attr('src');
		portrait = portrait.indexOf(URL.HTTP) < 0 ? "http://www.oschina.net" + portrait : portrait;

		var body = $(this).find('span.log').html();
		//处理表情图片
		var imgReg = /<img.*?(?:>|\/>)/gi;
		var srcReg = /src=[\'\"]?([^\'\"]*)[\'\"]?/i;
		var imgs = body.match(imgReg);
		if (imgs != null) {
			for (var i = 0; i < imgs.length; i++) {
				var src = imgs[i].match(srcReg);
				console.info(src);
				body = body.replace(src[1],"http://www.oschina.net"+src[1]);
			}
		}



		var time = $(this).find('span.time').html();
		var outlines = time.trim().split("(");
		var pubdate = outlines[0];
		var commentcount = $(this).find('span.time').find('a').text().match(/\d+/)[0];
		var t = {
			'id': id,
			'portrait': portrait,
			'author': username,
			'authorid': userid,
			'body': body,
			'appclient': 1,
			'commentcount': commentcount,
			'pubdate': pubdate,
			'imgsmall': '',
			'imgbig': ''
		};
		list[list.length] = t;
		if (!t.portrait) {
			t.portrait = Parse.defaultPhoto;
		}
	});
	$(d).remove();
	return list;
}

Ptweet.parseDetail = function(xml) {
	var tw = $(xml).find('tweet');
	var t = {
		'id': $(tw).children('id').text(),
		'body': $(tw).children('body').text(),
		'author': $(tw).children('author').text(),
		'authorid': $(tw).children('authorid').text(),
		'appclient': $(tw).children('appclient').text(),
		'commentcount': $(tw).children('commentCount').text(),
		'portrait': $(tw).children('portrait').text(),
		'pubdate': $(tw).children('pubDate').text(),
		'imgsmall': $(tw).children('imgSmall').text(),
		'imgbig': $(tw).children('imgBig').text()
	};
	if (!t.portrait) {
		t.portrait = Parse.defaultPhoto;
	}
	return t;
}

var Pcomment = {};

Pcomment.parse = function(xml) {
	var c = $(xml).find('comment');
	var c = {
		'id': $(c).children('id').text(),
		'portrait': $(c).children('portrait').text(),
		'author': $(c).children('author').text(),
		'authorid': $(c).children('authorid').text(),
		'content': $(c).children('content').text(),
		'pubdate': $(c).children('pubdate').text(),
		'appclient': $(c).children('appclient').text()
	};
	if (!c.portrait) {
		c.portrait = Parse.defaultPhoto;
	}
	return c;
}

Pcomment.parseList = function(xml) {
	var coms = $(xml).find('comment');
	var list = new Array();
	$(coms).each(function() {
		var c = {
			'id': $(this).children('id').text(),
			'portrait': $(this).children('portrait').text(),
			'author': $(this).children('author').text(),
			'authorid': $(this).children('authorid').text(),
			'content': $(this).children('content').text(),
			'pubdate': $(this).children('pubdate').text(),
			'appclient': $(this).children('appclient').text()
		};
		if (!c.portrait) {
			c.portrait = Parse.defaultPhoto;
		}
		list[list.length] = c;
	});
	return list;
}

var Pblog = {};

Pblog.parseList = function(xml) {
	var blogs = $(xml).find('blog');
	var list = new Array();
	$(blogs).each(function() {
		var b = {
			'id': $(this).children('id').text(),
			'title': $(this).children('title').text(),
			'url': $(this).children('url').text(),
			'pubdate': $(this).children('pubdate').text(),
			'authorid': $(this).children('authorid').text(),
			'authorname': $(this).children('authorname').text(),
			'commentcount': $(this).children('commentcount').text(),
			'documenttype': $(this).children('documenttype').text()
		};
		list[list.length] = b;
	});
	return list;
}