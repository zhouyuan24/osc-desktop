#osc-desktop

    oschina 的桌面客户端基于AlloyDesktop开发，目前只支持windows版本.
    
    当前版本 0.0.1-SNAPSHOT
    
####1.0版本计划功能
* 动弹查看与回复
* 快速回复评论、回复@、回复消息 
* 最新状态，关注关系处理等
* 推荐博客阅读、评论、回复
* 最新资讯、软件更新资讯
* 讨论区、技术问答

##AlloyDesktop

* [主页](http://webtop.alloyteam.com/) : http://webtop.alloyteam.com/ 
* [Git](https://github.com/AlloyTeam/webtop) : https://github.com/AlloyTeam/webtop