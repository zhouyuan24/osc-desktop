
var tweet = {
	width:600,
	height:500,
	handler :0,
	id : 0,
	uid:0,
	replyid:0,
	replyAuthorId :0
};

tweet.win = (function(){
	return {
		init : function(){
			document.onselectstart = function() {
				return false;
			};
			// 处理 窗体拖动事件
			win_head.onmousedown = function() {
				AlloyDesktop.drag();
			};
			win_head.onmouseup = function() {
				AlloyDesktop.stopDrag();
				console.log(AlloyDesktop.getPos());
			};
			u.onmousedown = function() {
				AlloyDesktop.drag();
			};
			u.onmouseup = function() {
				AlloyDesktop.stopDrag();
				console.log(AlloyDesktop.getPos());
			};
			win_head.onselectstart = function() {
				return false;
			};
			win_min_btn.onclick = function() {
				AlloyDesktop.mini();
			};
			win_close_btn.onclick = function() {
				AlloyDesktop.close();
			};
		}
	};
})();

tweet.view = (function(){
	return {
		init : function(){
			$('#comments').focus();
			api.getTweetDetail(tweet.id,function(data){
				var t = Ptweet.parseDetail(data);
				tweet.view.drawMain(t);

				api.getCommentList(3,tweet.id,0,t.commentcount,function(data){
					var list = Pcomment.parseList(data);
					var html = Templates.tweetcomment({'list':list});
					$('#comments').html(html);
					tweet.view.commentsEventBind();
				});

				api.getTweetList(t.authorid,0,10,function(data){
					var list = Ptweet.parseList(data);
					var html = Templates.tweet_reply_users({'list':list});
					$('#content_tweet').html(html);
					tweet.view.newTweetEventBind();
				});

				api.getUserBlogList(t.authorid,'','',0,10,function(data){
					var list = Pblog.parseList(data);
					var html = Templates.tweet_reply_users_blog({'list':list});
					$('#content_blog').html(html);
				});
			});

			tweet.view.eventBind();
		},

		drawMain : function(t){
			$('#u_name').html(t.author);
			$('#tbody').html(t.body);
			$('#tstat').html(t.pubdate+" 评论("+t.commentcount+")");
			$('.uphoto img').attr('src',t.portrait);
		},

		eventBind : function(){
			$('.tabs').find('div').each(function(){
				$(this).off().on('click',function(){
					if(!$(this).hasClass('select')){
						$('#tab_tweet').toggleClass('select');
						$('#tab_blog').toggleClass('select');
						$('#content_tweet').toggle();
						$('#content_blog').toggle();
					}
					
				});
			});

			$('#replybtn').off().on('click',function(){
				var content = $('#replycontent').val();
				if(tweet.replyid!=0){
					api.replyComment(tweet.id, 3, tweet.replyid,tweet.replyAuthorId,tweet.uid,content,function(data){
						var result = Parse.result(data);
						var comment = Pcomment.parse(data);
						var html = Templates.tweetcomment({'list':[comment]});
						$('#comments').prepend(html);
						tweet.view.commentsEventBind();
					});
				}else{
					api.pubComment(3,tweet.id,tweet.uid,content,0,function(data){
						var result = Parse.result(data);
						var comment = Pcomment.parse(data);
						var html = Templates.tweetcomment({'list':[comment]});
						$('#comments').prepend(html);
						tweet.view.commentsEventBind();
						
					});
				}
				tweet.replyid=0;
				tweet.replyAuthorId =0;
				$('#replycontent').val('');
			});
		},

		newTweetEventBind :function(){
			$('.utweet').each(function(){
				$(this).off().on('click',function(){
					tweet.id = $(this).attr('id');
					api.getTweetDetail(tweet.id,function(data){
						var t = Ptweet.parseDetail(data);
						tweet.view.drawMain(t);

						api.getCommentList(3,tweet.id,0,t.commentcount,function(data){
							var list = Pcomment.parseList(data);
							var html = Templates.tweetcomment({'list':list});
							$('#comments').html(html);
						});
					});
				});
			});
		},

		commentsEventBind : function(){
			$('.reply_a').each(function(){
				$(this).off().on('click',function(){
					var replyid = $(this).attr('replyid');
					tweet.replyid = replyid;
					tweet.replyAuthorId = $(this).attr('authorid');
					var content = $('#replycontent').val();
					var uname = $(this).parent().find('.uname').text();
					if(content.substring(0,2)!='回复'){
						content = '回复'+uname+": "+content;
					}else{
						var cc = content.split(':');
						var prename = cc[0].substring(2,cc[0].length);

						content=content.substring(cc[0].length+1);
						content =  '回复'+uname+":@"+prename+" "+content;
						if(content.indexOf('@'+uname+' ')>0){
							content = content.replace('@'+uname+' ','');
						}
					}
					$('#replycontent').val(content);
				});
			});
		}
	};
})();

tweet.init = (function(){
	return {
		init : function(){
			tweet.win.init();
			tweet.id = help.alloy.getParam('tweetid');
			tweet.uid =  help.alloy.getParam('uid');
			console.info(tweet.id);
			tweet.view.init();
		},
		readyHandler : function(){
			//alert(1);
			AlloyDesktop.hide();
			var win = AlloyDesktop.getScreenSize();
			var x = (win.width-tweet.width)/2;
			var y = (win.height-tweet.height)/2;
			AlloyDesktop.move(x,y);
			AlloyDesktop.setSize(tweet.width,tweet.height);
			AlloyDesktop.restore();
			AlloyDesktop.focus();
			//AlloyDesktop.enableResize();
			tweet.init.init();
		}
	};
})();