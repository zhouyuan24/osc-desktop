/**
 * 页面模板
 */
Templates = {
	tweets : template('<% for(var i=0; i<list.length; i++){ %>'
						+'<div class="tweet" id="<%=list[i].id%>">'
							+'<div class="u-img">'
								+'<img src="<%=list[i].portrait%>" width="38" height="38">'
							+'</div>'
							+'<div class="body">'
								+'<p class="u-name"><a href="#"><%=list[i].author%></a>：'
								+'<%=list[i].body%></p>'
								+'<span class="t-time"><%=list[i].pubdate%>(<%=list[i].commentcount%>评)</span>'
							+'</div>'
						+'</div>'
					+'<% } %>'
		),

	tweetcomment : template(
			'<% for(var i=0; i<list.length; i++){ %>'
				+'<div class="comment" id="<%=list[i].id%>">'
					+'<div class="c-photo">'
						+'<img src="<%=list[i].portrait%>" width="45" height="45">'
					+'</div>'
					+'<div class="c-uname">'
						+'<span class="uname"><%=list[i].author%></span>'
						+'<span class="c-time">(<%=list[i].pubdate%>)</span>'
							+'<a class="reply_a" href="#" authorid="<%=list[i].authorid%>" replyid="<%=list[i].id%>">回复</a>'
					+'</div>'
					+'<div class="c-info">'
						+'<p><%=list[i].content%></p>'
					+'</div>'
				+'</div>'
			+'<% } %>'
		),

	tweet_reply_users : template(
			'<% for(var i=0; i<list.length; i++){ %>'
				+'<div class="utweet" id="<%=list[i].id%>">'
					+'<p><%=list[i].body%> </p>'
					+'<% if (list[i].imgsmall!=""){ %>'
					+'<img src="<%=list[i].imgsmall%>">'
					+'<%}%>'
					+'<p class="utweet-time"><%=list[i].pubdate%>  评论(<%=list[i].commentcount%>)</p>'
				+'</div>'
			+'<% } %>'
		),

	tweet_reply_users_blog : template(
			'<% for(var i=0; i<list.length; i++){ %>'
				+'<div class="utweet" id="<%=list[i].id%>">'
					+'<p class="ublog-title"><%=list[i].title%> </p>'
					+'<p class="utweet-time"><%=list[i].pubdate%>  评论(<%=list[i].commentcount%>)</p>'
				+'</div>'
			+'<% } %>'
		)
};


