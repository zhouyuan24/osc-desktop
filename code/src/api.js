var api = {};

function _callback(data, callback) {
	if (callback && (callback instanceof Function))
		callback(data);
	else {
		return data;
	}
}

function _post(url, data, callback) {
	var async = (callback instanceof Function);
	console.info((async ? "ASYNC" : "") + " POST " + url);
	console.info(data);
	$.ajax({
		url: url,
		type: 'POST',
		async: async,
		data: data,
		success: function(data) {
			_callback(data, callback);
		}
	});
}

function _get(url, data, callback) {
	var async = (callback instanceof Function);
	if (url.indexOf("?") < 0) {
		url += "?";
	}
	for (var d in data) {
		url += "&" + d + "=" + data[d];
	}
	console.info((async ? "ASYNC" : "") + " GET " + url.replace("?&", "?"));
	$.ajax({
		url: url.replace("?&", "?"),
		type: 'GET',
		async: async,
		data: data,
		//dataType:'html/text',
		success: function(data) {
			_callback(data, callback);
		}
	});
}

api.login = function(username, password, callback) {
	var data = {
		"username": username,
		"pwd": password,
		"keep_login": 1
	};
	_post(URL.LOGIN_VALIDATE_HTTP, data, callback);
}

/**
 * 我的个人资料
 * @param uid
 */
api.myinformation = function(uid, callback) {
	var data = {
		uid: uid
	};
	_get(URL.MY_INFORMATION, data, callback);
}

/**
 * 获取用户信息个人专页（包含该用户的动态信息以及个人信息）
 * @param uid 自己的uid
 * @param hisuid 被查看用户的uid
 * @param hisname 被查看用户的用户名
 * @param pageIndex 页面索引
 * @param pageSize 每页读取的动态个数
 */
api.information = function(uid, hisuid, hisname, page, size, callback) {
	var data = {
		uid: uid,
		hisuid: hisuid,
		hisname: hisname,
		pageIndex: page,
		pageSize: size
	};
	_post(URL.USER_INFORMATION, data, callback);
}

/**
 * 更新用户之间关系（加关注、取消关注）
 * @param uid 自己的uid
 * @param hisuid 对方用户的uid
 * @param newrelation 0:取消对他的关注 1:关注他
 * @return
 * @throws AppException
 */
api.updateRelation = function(uid, hisuid, newrelation, callback) {
	var data = {
		uid: uid,
		hisuid: hisuid,
		newrelation: newrelation
	};
	_post(URL.USER_UPDATERELATION, data, callback);
}

/**
 * 获取用户通知信息
 * @param uid
 * @return
 * @throws AppException
 */
api.getUserNotice = function(uid, callback) {
	var data = {
		uid: uid
	};
	_post(URL.USER_NOTICE, data, callback);
}

/**
 * 清空通知消息
 * @param uid
 * @param type 1:@我的信息 2:未读消息 3:评论个数 4:新粉丝个数
 * @return
 * @throws AppException
 */
api.noticeClear = function(uid, type, callback) {
	var data = {
		uid: uid,
		type: type
	};
	_post(URL.NOTICE_CLEAR, data, callback);
}

/**
 * 用户粉丝、关注人列表
 * @param uid
 * @param relation 0:显示自己的粉丝 1:显示自己的关注者
 * @param pageIndex
 * @param pageSize
 * @return
 * @throws AppException
 */
api.getFriendList = function(uid, relation, page, size, callback) {
	var data = {
		uid: uid,
		relation: relation,
		pageIndex: page,
		pageSize: size
	};
	_post(URL.FRIENDS_LIST, data, callback);
}

/**
 * 获取资讯列表
 * @param url
 * @param catalog
 * @param pageIndex
 * @param pageSize
 * @return
 * @throws AppException
 */
api.getNewsList = function(catalog, page, size, callback) {
	var data = {
		catalog: catalog,
		pageIndex: page,
		pageSize: size
	};
	_post(URL.NEWS_LIST, data, callback);
}

/**
 * 获取资讯的详情
 * @param url
 * @param news_id
 * @return
 * @throws AppException
 */
api.getNewsDetail = function(newsid, callback) {
	var data = {
		id: newsid
	};
	_post(URL.NEWS_DETAIL, data, callback);
}

/**
 * 获取某用户的博客列表
 * @param authoruid
 * @param uid
 * @param pageIndex
 * @param pageSize
 * @return
 * @throws AppException
 */
api.getUserBlogList = function(authoruid, authorname, uid, page, size, callback) {
	var data = {
		authoruid: authoruid,
		authorname: authorname,
		uid: uid,
		pageIndex: page,
		pageSize: size
	};
	_post(URL.USERBLOG_LIST, data, callback);
}

/**
 * 获取博客列表
 * @param type 推荐：recommend 最新：latest
 * @param pageIndex
 * @param pageSize
 * @return
 * @throws AppException
 */
api.getBlogList = function(type, page, size, callback) {
	var data = {
		type: type,
		pageIndex: page,
		pageSize: size
	};
	_post(URL.BLOG_LIST, data, callback);
}

/**
 * 删除某用户的博客
 * @param uid
 * @param authoruid
 * @param id
 * @return
 * @throws AppException
 */
api.delBlog = function(uid, authoruid, id, callback) {
	var data = {
		uid: uid,
		authoruid: authoruid,
		id: id
	};
	_post(URL.USERBLOG_DELETE, data, callback);
}

/**
 * 获取博客详情
 * @param blog_id
 * @return
 * @throws AppException
 */
api.getBlogDetail = function(blogid, callback) {
	var data = {
		id: blogid
	};
	_post(URL.BLOG_DETAIL, data, callback);
}

/**
 * 获取帖子列表
 * @param url
 * @param catalog
 * @param pageIndex
 * @return
 * @throws AppException
 */
api.getPostList = function(catalog, page, size, callback) {
	var data = {
		catalog: catalog,
		pageIndex: page,
		pageSize: size
	};
	_post(URL.POST_LIST, data, callback);
}
/**
 * 通过Tag获取帖子列表
 * @param url
 * @param catalog
 * @param pageIndex
 * @return
 * @throws AppException
 */
api.getPostListByTag = function(tag, page, size, callback) {
	var data = {
		tag: tag,
		pageIndex: page,
		pageSize: size
	};
	_post(URL.POST_LIST, data, callback);
}
/**
 * 获取帖子的详情
 * @param url
 * @param post_id
 * @return
 * @throws AppException
 */
api.getPostDetail = function(postid, callback) {
	var data = {
		id: postid
	};
	_post(URL.POST_DETAIL, data, callback);
}
/**
 * 发帖子
 * @param post （uid、title、catalog、content、isNoticeMe）
 * @return
 * @throws AppException
 */
api.pubPost = function(post, callback) {
	_post(URL.POST_PUB, post, callback);
}

/**
 * 获取我的动弹列表
 * @param uid
 * @param pageIndex
 * @param pageSize
 * @return
 * @throws AppException
 */
api.getTweetList = function(uid, page, size, callback) {
	var data = {
		uid: uid,
		pageIndex: page,
		pageSize: size
	};
	_post(URL.TWEET_LIST, data, callback);
}
/**
 * 获取所有动弹列表
 * @param p  默认每页返回40条啊，有点多啊啊啊啊啊，红薯求api啊啊啊啊啊。。。。
 * @return
 * @throws AppException
 */
api.getNewTweetList = function(p, callback) {
	var data = {
		p: p
	};
	_get(URL.NEWTWEET_LIST, data, callback);
}

/**
 * 获取最新动弹列表
 * @param last : 动弹id，只能返回5条啊啊啊啊。 红薯求api啊啊啊啊啊。。。。
 * @return
 * @throws AppException
 */
api.getLastTweetList = function(last, callback) {
	var data = {
		last: last
	};
	_get(URL.LASTTWEET_LIST, data, callback);
}

/**
 * 获取动弹详情
 * @param tweet_id
 * @return
 * @throws AppException
 */
api.getTweetDetail = function(tweet_id, callback) {
	var data = {
		id: tweet_id
	};
	_get(URL.TWEET_DETAIL, data, callback);
}

/**
 * 发动弹
 * @param Tweet-uid & msg & image
 * @return
 * @throws AppException
 */
//TODO 发动弹
api.pubTweet = function(uid, msg, callback) {
	var data = {
		uid:uid,
		msg : msg
	};
	_post(URL.TWEET_PUB, data, callback);
}

/**
 * 删除动弹
 * @param uid
 * @param tweetid
 * @return
 * @throws AppException
 */
api.delTweet = function(uid, tweetid, callback) {
	var data = {
		uid: uid,
		tweetid: tweetid
	};
	_post(URL.TWEET_DELETE, data, callback);
}

/**
 * 获取动态列表
 * @param uid
 * @param catalog 1最新动态  2@我  3评论  4我自己
 * @param pageIndex
 * @param pageSize
 * @return
 * @throws AppException
 */

api.getActiveList = function(uid, catalog, page, size) {
	var data = {
		uid: uid,
		catalog: catalog,
		pageIndex: page,
		pageSize: size
	};
	_get(URL.ACTIVE_LIST, data, callback);
}

/**
 * 获取留言列表
 * @param uid
 * @param pageIndex
 * @return
 * @throws AppException
 */
api.getMessageList = function(uid, page, size) {
	var data = {
		uid: uid,
		pageIndex: page,
		pageSize: size
	};
	_get(URL.MESSAGE_LIST, data, callback);
}

/**
 * 发送留言
 * @param uid 登录用户uid
 * @param receiver 接受者的用户id
 * @param content 消息内容，注意不能超过250个字符
 * @return
 * @throws AppException
 */
api.pubMessage = function(uid, receiver, content, callback) {
	var data = {
		uid: uid,
		receiver: receiver,
		content: content
	};
	_post(URL.MESSAGE_PUB, data, callback);
}

/**
 * 转发留言
 * @param uid 登录用户uid
 * @param receiver 接受者的用户名
 * @param content 消息内容，注意不能超过250个字符
 * @return
 * @throws AppException
 */
api.forwardMessage = function(uid, receiverName, content, callback) {
	var data = {
		uid: uid,
		receiverName: receiverName,
		content: content
	};
	_post(URL.MESSAGE_PUB, data, callback);
}

/**
 * 删除留言
 * @param uid 登录用户uid
 * @param friendid 留言者id
 * @return
 * @throws AppException
 */
api.delMeesage = function(uid, friendid, callback) {
	var data = {
		uid: uid,
		friendid: friendid
	};
	_post(URL.MESSAGE_DELETE, data, callback);
}

/**
 * 获取博客评论列表
 * @param id 博客id
 * @param pageIndex
 * @param pageSize
 * @return
 * @throws AppException
 */
api.getBlogCommentList = function(id, page, size, callback) {
	var data = {
		id: id,
		pageIndex: page,
		pageSize: size
	};
	_get(URL.BLOGCOMMENT_LIST, data, callback);
}

/**
 * 发表博客评论
 * @param blog 博客id
 * @param uid 登陆用户的uid
 * @param content 评论内容
 * @return
 * @throws AppException
 */
api.pubBlogComment = function(blog, uid, content, callback) {
	var data = {
		blog: blog,
		uid: uid,
		content: content
	};
	_post(URL.BLOGCOMMENT_PUB, data, callback);
}

/**
 * 回复博客评论
 * @param blog 博客id
 * @param uid 登陆用户的uid
 * @param content 评论内容
 * @param reply_id 评论id
 * @param objuid 被评论的评论发表者的uid
 * @return
 * @throws AppException
 */
api.replyBlogComment = function(blog, uid, content, reply_id, objuid, callback) {
	var data = {
		blog: blog,
		uid: uid,
		content: content,
		reply_id: reply_id,
		objuid: objuid
	};
	_post(URL.BLOGCOMMENT_PUB, data, callback);
}

/**
 * 删除博客评论
 * @param uid 登录用户的uid
 * @param blogid 博客id
 * @param replyid 评论id
 * @param authorid 评论发表者的uid
 * @param owneruid 博客作者uid
 * @return
 * @throws AppException
 */
api.delBlogComment = function(uid, blogid, replyid, authorid, owneruid, callback) {
	var data = {
		uid: uid,
		blogid: blogid,
		replyid: replyid,
		authoruid: authoruid,
		owneruid: owneruid
	};
	_post(URL.BLOGCOMMENT_DELETE, data, callback);
}

/**
 * 获取评论列表
 * @param catalog 1新闻  2帖子  3动弹  4动态
 * @param id
 * @param pageIndex
 * @param pageSize
 * @return
 * @throws AppException
 */
api.getCommentList = function(catalog, id, page, size, callback) {
	var data = {
		catalog: catalog,
		id: id,
		pageIndex: page,
		pageSize: size
	};
	_get(URL.COMMENT_LIST, data, callback);
}

/**
 * 发表评论
 * @param catalog 1新闻  2帖子  3动弹  4动态
 * @param id 某条新闻，帖子，动弹的id
 * @param uid 用户uid
 * @param content 发表评论的内容
 * @param isPostToMyZone 是否转发到我的空间  0不转发  1转发
 * @return
 * @throws AppException
 */
api.pubComment = function(catalog, id, uid, content, isPostToMyZone, callback) {
	var data = {
		catalog: catalog,
		id: id,
		uid: uid,
		content: content,
		isPostToMyZone: isPostToMyZone
	};
	_post(URL.COMMENT_PUB, data, callback);
}


/**
 *
 * @param id 表示被评论的某条新闻，帖子，动弹的id 或者某条消息的 friendid
 * @param catalog 表示该评论所属什么类型：1新闻  2帖子  3动弹  4动态
 * @param replyid 表示被回复的单个评论id
 * @param authorid 表示该评论的原始作者id
 * @param uid 用户uid 一般都是当前登录用户uid
 * @param content 发表评论的内容
 * @return
 * @throws AppException
 */
api.replyComment = function(id, catalog, replyid, authorid, uid, content, callback) {
	var data = {
		id: id,
		catalog: catalog,
		replyid: replyid,
		authorid: authorid,
		uid: uid,
		content: content
	};
	_post(URL.COMMENT_REPLY, data, callback);
}