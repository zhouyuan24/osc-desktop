
var osc = {
	appname:'osc desktop',
	version : '0.0.1',
	username : "",
	password : "",
	lasttweet : 0,
	tweet_refresh_time:3000,
	handlers:new Array(),
	login :0
};

osc.timers = {
	tweetid : 0,
	hottweet : 0
};

osc.user ={};

osc.cfg = {
	width :0,
	height :0,
	minwidth :280,
	minheight :400
	
};


osc.handler = (function(){
	return {
		clear : function(){
			for(var i in osc.handlers){
				AlloyDesktop.close(osc.handlers[i]);
			}
		},
		remove : function(id){
			for(var i in osc.handlers){
				if(osc.handlers[i] == id)
					osc.handlers.splice(i,1);
			}
		}
	};
})();


osc.win = (function(){
	return {
		init : function(){
			document.onselectstart = function() {
				return false;
			};
			win_head.ondblclick = function() {
				$(app_max_btn).click();
			};
			// 处理 窗体拖动事件
			win_head.onmousedown = function() {
				AlloyDesktop.drag();
			};
			win_head.onmouseup = function() {
				AlloyDesktop.stopDrag();
				console.info(AlloyDesktop.getPos());
			};
			win_head.onselectstart = function() {
				return false;
			};
			win_min_btn.onclick = function() {
				AlloyDesktop.hide();
			};
			win_close_btn.onclick = function() {
				AlloyDesktop.delTaskIcon(osc.appname);
				//osc.handler.clear();
				AlloyDesktop.quit();
			};

			osc.view.initView();
		}
	};
})();

osc.view = (function(){
	return {
		initView : function(){
			osc.view.loginEventBind();
			osc.view.viewEventBind();
			osc.login = Util.local.get("autologin");
			if(osc.login == "true"){
				osc.username = Util.local.get("username");
				osc.password = Util.local.get("password");
				osc.view.login();
			}
		},

		showView:function(){
			$('.login').toggle();
			$('.user-info').toggle();
			$('.search').toggle();
			$('.menu-tabs').toggle();
			$('.win-center').toggle();
			$('.win-bottom').toggle();
		},

		afterLogn : function(){
			osc.view.initUserInfo();
			osc.view.drawTweets();
			osc.view.drawHotTweets();
			osc.view.showView();
		},

		loginEventBind : function(){
			$('#username').off().on('focus',function(){
				if($(this).val()=='Email')
					$(this).val('');
			}).on('blur',function(){
				if($(this).val()=='')
					$(this).val('Email');
			});
			$('#loginbtn').off().on('click',function(){
				var username = $('#username').val();
				var password = $('#password').val();
				var autologin = $('#autologin')[0].checked;
				Util.local.put("autologin",autologin);
				Util.local.put("username",username);
				Util.local.put("password",password);
				osc.username = username;
				osc.password = password;
				osc.view.login();
			});
		},

		login : function(){
			api.login(osc.username,osc.password,function(data){
					var result = Parse.result(data);
					if(result.code==1){
						osc.user = user.parse(data);
						osc.login=1;
						osc.view.afterLogn();
						osc.timer.init();
					}else{
						$('#loginmsg').html(result.message);
						$('#loginmsg').show();
					}
				});
		},

		initUserInfo : function(){
			$('#user-name').html(osc.user.name);
			$('#user-photo-img').attr("src",osc.user.portrait);
			$('#user-stat-followers').html('关注('+osc.user.followers+')');
			$('#user-stat-fans').html('粉丝('+osc.user.fans+')');
			$('#user-stat-score').html('积分('+osc.user.score+')');

		},

		drawTweets : function(){
			api.getTweetList(0,0,20,function(data){
				var list = Ptweet.parseList(data);
				if(list.length>0){
					osc.lasttweet = list[0].id;
					var html = Templates.tweets({'list':list});
					$('#tweets').empty();
					$('#tweets').append(html);
					osc.view.tweetsEventBind();
				}
			});
		},
		tweetsEventBind:function(){
			$('.tweet').each(function(){
				$(this).off().on('dblclick',function(){
					//alert($(this).attr('id'));
					console.info(AlloyDesktop.getCurrentDirectory());
					var data = {'tweetid':$(this).attr('id'),'uid':osc.user.uid};
					var h = help.alloy.createWindow('src/views/TweetDetail.html',data);
					//var h = AlloyDesktop.createWindow(AlloyDesktop.getCurrentDirectory()+'src/views/TweetDetail.html','',1);
					osc.handlers.unshift(h);
				});
			});
		},

		drawHotTweets : function(){
			api.getTweetList(-1,0,20,function(data){
				var list = Ptweet.parseList(data);
				if(list.length>0){
					var html = Templates.tweets({'list':list});
					$('#hottweets').empty();
					$('#hottweets').append(html);
					osc.view.tweetsEventBind();
				}
			});
		},

		viewEventBind : function(){
			$('.menu-tab').each(function(){
				$(this).off().on('click',function(){
					$('.menu-tab').each(function(){
						$(this).removeClass('select');
						var target = $(this).attr('target');
						$('#'+target).hide();
					});
					$(this).addClass('select');
					$('#'+$(this).attr('target')).show();
				});
			});

			$("*").each(function(){
				$(this).poshytip({alignY: 'bottom',alignX: 'right',offsetY: 15,offsetX: 10});
			});

			$('#newtweet').click(function(){
				var data = {'uid' : osc.user.uid};
				help.alloy.createWindow('src/views/TweetPub.html',data);
			});
		}

	};
})();

osc.timer = (function(){
	return {
		init: function(){
			osc.timer.tweet();
			osc.timer.hottweet();
		},
		clear: function(){
			clearInterval(osc.timers.tweetid);
			clearInterval(osc.timers.hottweet);
		},
		tweet:function(){
			osc.timers.tweetid = setInterval(function(){

				if(osc.lasttweet!=0){
					api.getLastTweetList(osc.lasttweet,function(data){

						clearInterval(osc.timers.tweetid);

						var list =  Ptweet.parseNewListFromHtml(data);
						if(list.length>0){
							var lastid = $('#tweets').find('.tweet:first').attr('id');
							lastid = (lastid)? lastid : 0;
							var tlist = new Array();
							for(var i in list){
								if(list[i].id>lastid)
									tlist[tlist.length] = list[i];
							}
							osc.lasttweet = tlist.length>0 ? tlist[0].id : lastid;
							var html = Templates.tweets({'list':tlist});
							$('#tweets').prepend(html);
							osc.view.tweetsEventBind();
						}

						osc.timer.tweet();
					});
				}
				
			},osc.tweet_refresh_time);
		},
		hottweet : function(){
			osc.timers.hottweet = setInterval(function(){
				api.getTweetList(-1,0,20,function(data){
					clearInterval(osc.timers.hottweet);
					var list = Ptweet.parseList(data);
					if(list.length>0){
						var html = Templates.tweets({'list':list});
						$('#hottweets').empty();
						$('#hottweets').append(html);
						osc.view.tweetsEventBind();
					}
					osc.timer.hottweet();
				});
			},1000*60);
		}
	};
})();

osc.init = (function(){

	var relayoutWin = function() {
		$('.bg').css('height', osc.cfg.height + 'px');
		$('.win-center').css('height', (osc.cfg.height-203) + 'px');
	};

	return {
		readyHandler:function(){
			AlloyDesktop.setTaskIcon(osc.appname, 'osc.ico', osc.appname);

			var screenSize = AlloyDesktop.getScreenSize();
			//AlloyDesktop.move(screenSize.width-500, 100);
			osc.cfg.width = screenSize.width;
			osc.cfg.height =screenSize.height;
			osc.win.init();
		},
		sizeHandler : function(e) {
			var w = e.detail.width, h = e.detail.height;
			// container.style.height = h - 12 + 'px';
			// if(w < 900 || h < 600) {
			// 	AlloyDesktop.setSize(900, 600);
			// }
			osc.cfg.width = w;
			osc.cfg.height = h;
			relayoutWin();

		},
		focusHandler : function() {
			
		},
		activeHandler : function(x, y) {
		},
		taskMouseHandler : function(e) {
			
			if(e.detail.type == '513') {
				AlloyDesktop.restore();
				AlloyDesktop.bringToTop();
				AlloyDesktop.focus();
				console.info(e.detail.type);
			}
		},
		windowCloseHandler : function(){
			//osc.handler.clear();
			AlloyDesktop.delTaskIcon(osc.appname);
			AlloyDesktop.quit();
		},
		dragDropHandler : function(e) {
			console.log(e.detail);
			if(Rest.cfg.currentWin == "music") {
				var files = e.detail.list;
				music.playList.length = 0;
				for(var i = 0, size = files.length; i < size; i++) {
					var file = files[i];
					var name = file.split("/");
					name = name[name.length - 1].split(".")[0];
					var i = 0;
					var m = AlloyDesktop.createMemory("temp", file, i);
					var s = AlloyDesktop.createStream(m);
					console.log(s);
					console.log(i);
				}
				;
				// Rest.music.init();
			}
		}
	};
})();

