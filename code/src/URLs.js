var URL = {
	HOST: "www.oschina.net",
	HTTP: "http://",
	HTTPS: "https://",

	URL_SPLITTER: "/",
	URL_UNDERLINE: "_"
}

URL.URL_API_HOST = URL.HTTP + URL.HOST + URL.URL_SPLITTER;

URL.LOGIN_VALIDATE_HTTP = URL.URL_API_HOST + "action/api/login_validate";
URL.LOGIN_VALIDATE_HTTPS = URL.HTTPS + URL.HOST + URL.URL_SPLITTER + "action/api/login_validate";

URL.NEWS_LIST = URL.URL_API_HOST + "action/api/news_list";
URL.NEWS_DETAIL = URL.URL_API_HOST + "action/api/news_deta;il";

URL.POST_LIST = URL.URL_API_HOST + "action/api/post_list";;
URL.POST_DETAIL = URL.URL_API_HOST + "action/api/post_deta;il";
URL.POST_PUB = URL.URL_API_HOST + "action/api/post_pub";

URL.TWEET_LIST = URL.URL_API_HOST + "action/api/tweet_list";
URL.TWEET_DETAIL = URL.URL_API_HOST + "action/api/tweet_detail";
URL.TWEET_PUB = URL.URL_API_HOST + "action/api/tweet_pub";
URL.TWEET_DELETE = URL.URL_API_HOST + "action/api/tweet_delete";

URL.NEWTWEET_LIST = URL.URL_API_HOST + "fetch_tweets";
URL.LASTTWEET_LIST = URL.URL_API_HOST + "widgets/check-top-log";


URL.ACTIVE_LIST = URL.URL_API_HOST + "action/api/active_list";

URL.MESSAGE_LIST = URL.URL_API_HOST + "action/api/message_list";
URL.MESSAGE_DELETE = URL.URL_API_HOST + "action/api/message_delete";
URL.MESSAGE_PUB = URL.URL_API_HOST + "action/api/message_pub";
URL.COMMENT_LIST = URL.URL_API_HOST + "action/api/comment_list";
URL.COMMENT_PUB = URL.URL_API_HOST + "action/api/comment_pub";
URL.COMMENT_REPLY = URL.URL_API_HOST + "action/api/comment_reply";
URL.COMMENT_DELETE = URL.URL_API_HOST + "action/api/comment_delete";
URL.SOFTWARECATALOG_LIST = URL.URL_API_HOST + "action/api/softwarecatalog_list";
URL.SOFTWARETAG_LIST = URL.URL_API_HOST + "action/api/softwaretag_list";
URL.SOFTWARE_LIST = URL.URL_API_HOST + "action/api/software_list";
URL.SOFTWARE_DETAIL = URL.URL_API_HOST + "action/api/software_detail";
URL.USERBLOG_LIST = URL.URL_API_HOST + "action/api/userblog_list";
URL.USERBLOG_DELETE = URL.URL_API_HOST + "action/api/userblog_delete";
URL.BLOG_LIST = URL.URL_API_HOST + "action/api/blog_list";
URL.BLOG_DETAIL = URL.URL_API_HOST + "action/api/blog_detail";
URL.BLOGCOMMENT_LIST = URL.URL_API_HOST + "action/api/blogcomment_list";
URL.BLOGCOMMENT_PUB = URL.URL_API_HOST + "action/api/blogcomment_pub";
URL.BLOGCOMMENT_DELETE = URL.URL_API_HOST + "action/api/blogcomment_delete";
URL.MY_INFORMATION = URL.URL_API_HOST + "action/api/my_information";
URL.USER_INFORMATION = URL.URL_API_HOST + "action/api/user_information";
URL.USER_UPDATERELATION = URL.URL_API_HOST + "action/api/user_updaterelation";
URL.USER_NOTICE = URL.URL_API_HOST + "action/api/user_notice";
URL.NOTICE_CLEAR = URL.URL_API_HOST + "action/api/notice_clear";
URL.FRIENDS_LIST = URL.URL_API_HOST + "action/api/friends_list";
URL.FAVORITE_LIST = URL.URL_API_HOST + "action/api/favorite_list";
URL.FAVORITE_ADD = URL.URL_API_HOST + "action/api/favorite_add";
URL.FAVORITE_DELETE = URL.URL_API_HOST + "action/api/favorite_delete";
URL.SEARCH_LIST = URL.URL_API_HOST + "action/api/search_list";
URL.PORTRAIT_UPDATE = URL.URL_API_HOST + "action/api/portrait_update";
URL.UPDATE_VERSION = URL.URL_API_HOST + "MobileAppVersion.xml";
URL.URL_HOST = "oschina.net";
URL.URL_WWW_HOST = "www." + URL.URL_HOST;
URL.URL_MY_HOST = "my." + URL.URL_HOST;
URL.URL_TYPE_NEWS = URL.URL_WWW_HOST + URL.URL_SPLITTER + "news" + URL.URL_SPLITTER;
URL.URL_TYPE_SOFTWARE = URL.URL_WWW_HOST + URL.URL_SPLITTER + "p" + URL.URL_SPLITTER;
URL.URL_TYPE_QUESTION = URL.URL_WWW_HOST + URL.URL_SPLITTER + "question" + URL.URL_SPLITTER;
URL.URL_TYPE_BLOG = URL.URL_SPLITTER + "blog" + URL.URL_SPLITTER;
URL.URL_TYPE_TWEET = URL.URL_SPLITTER + "tweet" + URL.URL_SPLITTER;
URL.URL_TYPE_ZONE = URL.URL_MY_HOST + URL.URL_SPLITTER + "u" + URL.URL_SPLITTER;
URL.URL_TYPE_QUESTION_TAG = URL.URL_TYPE_QUESTION + "tag" + URL.URL_SPLITTER;
URL.URL_OBJ_TYPE_OTHER = 0x000;
URL.URL_OBJ_TYPE_NEWS = 0x001;
URL.URL_OBJ_TYPE_SOFTWARE = 0x002;
URL.URL_OBJ_TYPE_QUESTION = 0x003;
URL.URL_OBJ_TYPE_ZONE = 0x004;
URL.URL_OBJ_TYPE_BLOG = 0x005;
URL.URL_OBJ_TYPE_TWEET = 0x006;
URL.URL_OBJ_TYPE_QUESTION_TAG = 0x007;